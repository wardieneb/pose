class Config {
  static const String appName = "pose";
  static const String apiURL = 'http://localhost/api:8080'; //PROD_URL
  static const loginAPI = "/users/login";
  static const registerAPI = "/users/signup";
  static const userProfileAPI = "/users/user-Profile";
}
